<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2014-2018 Daniel P. Berrange <dan@berrange.com> -->
<component type="desktop-application">
  <id>org.entangle_photo.Manager</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0+</project_license>
  <name>Entangle</name>
  <summary>Tethered Camera Control &amp; Capture</summary>
  <description>
    <p>
      Entangle is a program used to control digital cameras
      that are connected to the computer via USB.
    </p>
    <p>
      Entangle can trigger the camera shutter to capture new images.
      When supported by the camera, a continuously updating preview
      of the scene can be displayed prior to capture. Images will be
      downloaded and displayed as they are captured by the camera.
      Entangle also allows the settings of the camera to be changed
      from the computer.
    </p>
    <p>
      Entangle is compatible with most DSLR cameras from Nikon and
      Canon, some of their compact camera models, and a variety of
      cameras from other manufacturers.
    </p>
  </description>
  <launchable type="desktop-id">org.entangle_photo.Manager.desktop</launchable>
  <keywords>
    <keyword>Capture</keyword>
    <keyword>Camera</keyword>
    <keyword>Tethered</keyword>
    <keyword>Photo</keyword>
  </keywords>
  <screenshots>
    <screenshot type="default">http://entangle-photo.org/appdata/en_US/screenshots/camera-manager.png</screenshot>
  </screenshots>
  <url type="homepage">http://entangle-photo.org/</url>
  <developer_name>The Entangle Photo project</developer_name>
  <updatecontact>https://groups.google.com/forum/#!forum/entangle-devel</updatecontact>
  <content_rating type="oars-1.1" />
  <releases>
    <release version="2.0" codename="Sodium" date="2019-01-13">
      <description>
        <ul>
          <li>Require gobject introspection >= 1.54</li>
          <li>Require GTK3 >= 3.22</li>
          <li>Fix dependency on libraw</li>
          <li>Fix variable name in photobox plugin</li>
          <li>Document some missing keyboard shortcuts</li>
          <li>Fix upper bound in histogram to display clipped pixel</li>
          <li>Refresh translations</li>
          <li>Option to highlight over exposed pixels in red</li>
          <li>Disable noisy compiler warning</li>
          <li>Remove use of deprecated application menu concept</li>
          <li>Fix image redraw when changing some settings</li>
          <li>Update mailing list address in appdaat</li>
          <li>Add more fields to appdata content</li>
          <li>Fix reference counting during window close</li>
          <li>Use correct API for destroying top level windows</li>
          <li>Fix unmounting of cameras with newer gvfs URI naming scheme</li>
          <li>Avoid out of bounds read of property values</li>
          <li>Fix many memory leaks</li>
          <li>Workaround for combo boxes not displaying on Wayland</li>
          <li>Fix race condition in building enums</li>
          <li>Fix setting of gschema directory during startup</li>
          <li>Set env to ensure plugins can find introspection typelib</li>
        </ul>
      </description>
    </release>
    <release version="1.0" codename="Lithium" date="2017-10-10">
      <description>
        <ul>
          <li>Requires Meson + Ninja build system instead of make</li>
          <li>Switch to 2-digit version numbering</li>
          <li>Fix corruption of display when drawing session browser</li>
          <li>Register application actions for main operations</li>
          <li>Compile UI files into binary</li>
          <li>Add a custom application menu</li>
          <li>Switch over to using header bar, instead of menu bar and tool bar.</li>
          <li>Enable close button for about dialog</li>
          <li>Ensure plugin panel fills preferences dialog</li>
          <li>Tweak UI spacing in supported cameras dialog</li>
          <li>Add keyboard shortcuts overlay</li>
        </ul>
      </description>
    </release>
    <release version="0.7.2" codename="Bottom" date="2017-08-25">
      <description>
        <ul>
          <li>Requires Gtk >= 3.10.0</li>
          <li>Fix some introspection annotations</li>
          <li>Use GdkSeat APIs if available</li>
          <li>Use GtkOverlay and GtkRevealer in preference to custom widgets</li>
          <li>Refactoring to prepare to support display of video files</li>
          <li>Draw symbolic icons for video/image files while waiting for thumbnails to load</li>
          <li>Ensure session highlight has a min 1 pixel visible border</li>
          <li>Ensure session browser scrolls fully to right</li>
          <li>Check for Adwaita icon theme which now includes symbolic icons</li>
          <li>Remove left over check for DBus GLib</li>
          <li>Remove use of deprecated GDK monitor functions</li>
          <li>Remove use of deprecated GTK API for loading URIs</li>
          <li>Fix handling of motion-notify event that broke client side window dragging</li>
          <li>Fix warning when setting size of settings viewport</li>
          <li>Update bug reporting address</li>
          <li>Turn off over-zealous compiler warning about loop optimizations</li>
          <li>Add ability to enter IP address of network camera</li>
          <li>Fix URI pattern used to locate gphoto gvfs mounts</li>
          <li>Add example plugin for bracketing photos of a total eclipse</li>
        </ul>
      </description>
    </release>
    <release version="0.7.1" codename="Top" date="2016-02-21">
      <description>
        <ul>
          <li>Fix linking problem with strict linkers</li>
          <li>Misc spelling fixes to online help docs</li>
          <li>Replace use of GSimpleAsyncResult with GTask</li>
          <li>Specify versions when importing from python plugins</li>
          <li>Remove use of deprecated GTK APIs</li>
          <li>Render image stats overlay partially transparent</li>
          <li>Fix error reporting when saving settings</li>
          <li>Flush events after capture to avoid accidentally restarting preview</li>
          <li>Make Nikon fine focus stepping finer</li>
          <li>Ensure images are sorted by last modified date</li>
          <li>Switch from 128 px to 256 px thumbnail sizes to benefit larger high dpi screens</li>
          <li>Rewrite film strip browser to dynamically resize icons to fit available space</li>
          <li>Draw symbolic icons in film strip if image is not yet loaded</li>
          <li>Refresh translations from Zanata</li>
        </ul>
      </description>
    </release>
    <release version="0.7.0" codename="Charm" date="2015-03-17">
      <description>
        <ul>
          <li>Require GLib >= 2.36</li>
          <li>Import new logo design</li>
          <li>Switch to using zanata.org for translations</li>
          <li>Set default window icon</li>
          <li>Introduce an initial help manual via yelp</li>
          <li>Use shared library for core engine to ensure all symbols are exported to plugins</li>
          <li>Add framework for scripting capture operations</li>
          <li>Workaround camera busy problems with Nikon cameras</li>
          <li>Add a plugin for repeated capture sequences</li>
          <li>Replace progress bar with spinner icon</li>
        </ul>
      </description>
    </release>
    <release version="0.6.1" codename="Strange" date="2015-02-05">
      <description>
        <ul>
          <li>Require GTK >= 3.4</li>
          <li>Fix check for GIO package in configure</li>
          <li>Add missing icons to Makefile</li>
          <li>Follow freedesktop thumbnail standard storage location</li>
          <li>Refactor capture code to facilitate plugin script automation</li>
          <li>Fix bug causing plugin to be displayed more than once</li>
          <li>Make histogram height larger</li>
          <li>Strip trailing '2' from widget labels to be more friendly</li>
          <li>Completely rewrite control panel display to show a small, user configurable subset from all the camera controls.</li>
          <li>Remember custom list of camera controls per camera model</li>
          <li>Hide compiler warnings from new glib atomic opertaions</li>
          <li>Update to newer gnulib compiler warnings code</li>
          <li>Remove broken double buffering code that's no required when using GTK3</li>
          <li>Remove use of deprecated GtkMisc APis</li>
          <li>Allow camera picker list to show multiple lines</li>
          <li>Remove crufty broken code from session browser that was breaking with new GTK versions</li>
          <li>Disable libraw auto brightness since it totally overexposes many images, generally making things look worse</li>
          <li>Fix memory leak handling camera events</li>
          <li>Add keywords to desktop &amp; appdata files</li>
        </ul>
      </description>
    </release>
    <release version="0.6.0" codename="Down" date="2014-05-02">
      <description>
        <ul>
          <li>Add a demonstration plugin for setting up a captive photo box display mode</li>
          <li>Switch to use Python3 for plugin engine instead of JavaScript</li>
          <li>Use GTK dark theme</li>
          <li>Require GNOME symbolic icon theme to be installed</li>
          <li>Switch to require lcms2 instead of lcms</li>
          <li>Move application icon into a standard directory mandated to work with the appdata tools</li>
          <li>Make manual focus work with Canon EOS cameras</li>
          <li>Disable flickering progress bar in preview mode with Canon EOS cameras</li>
          <li>Remove use of deprecated GTK methods/classes/constants</li>
          <li>Remove use of gexiv2 method which is not long exported</li>
          <li>Remove use of deprecated libpeas methods</li>
          <li>Add GTK-DOC transfer annotations / docs to all methods</li>
          <li>Avoid loosing camera capabilities on disconnect</li>
          <li>Fix off by one in histogram tables causing memory corruption</li>
          <li>Mark appdata / desktop files for translation</li>
          <li>Fix typos in README file</li>
          <li>Fix inverted tests when checking if range widget changed</li>
          <li>Avoid storm of expose events due to auto-drawer widget</li>
          <li>Avoid never ending circular update of controls causing errors in some camera modes</li>
          <li>Add workaround for crazy D5100 camera serial number</li>
          <li>Add customizable highlight/background for images</li>
          <li>Avoid reference leak of windows preventing proper cleanup</li>
          <li>Remove camera manual/about/driver help windows since it did not contain any info useful to users</li>
          <li>Filter list of cameras in connect dialog to only those which support capture/preview</li>
          <li>Don't auto connect to cameras which don't support capture or preview</li>
          <li>Ensure parent window is set on dialogs to prevent them falling behind main window</li>
          <li>Fix crash with latest GTK due to incorrect overriding of GtkApplication startup method</li>
          <li>Update to cope with changed GExiv API version</li>
          <li>Refreshed translations from transifex</li>
        </ul>
      </description>
    </release>
    <release version="0.5.4" codename="Up" date="2013-12-15">
      <description>
        <ul>
          <li>Ensure thumbnail directory exists when saving thumbnails</li>
          <li>Fix memory leak generating thumbnails from raw images</li>
          <li>Add an appdata XML file for GNOME software center</li>
          <li>Create a dedicated camera actions menu</li>
          <li>Fix include of gexiv2 header files</li>
          <li>Fix image selection for onion skinning in preview mode</li>
          <li>Display selected image when cancelling preview</li>
          <li>Fix empty tooltips on capture/preview buttons</li>
          <li>Fix scaling for preview image in onion skinning mode</li>
          <li>Increase emphasis of top image in onion skinning mode</li>
          <li>Improve granularity of manual focus and add extra '&lt;' and '&gt;' key accelerators for coarse focus</li>
          <li>Fix leak of image exiting preview mode</li>
          <li>Make progress toolbar button permanently visible</li>
        </ul>
      </description>
    </release>
    <release version="0.5.3" codename="Photon" date="2013-08-28">
      <description>
        <ul>
          <li>Move file name of image thumbnails to tooltip popup</li>
          <li>Add --disable-schemas-compile configure arg to disable schema compilation</li>
          <li>Remove "Encoding" key from desktop file</li>
          <li>Fix massive memory leak in pixbuf loading</li>
          <li>Fix thread safety in camera control updates which caused hangs in GTK</li>
          <li>Fix updating of control readonly state when camera modes change</li>
        </ul>
      </description>
    </release>
    <release version="0.5.2" codename="Graviton" date="2013-08-17">
      <description>
        <ul>
          <li>Split histogram into RGB colour channels</li>
          <li>Fix aperture / shutter speed calculations in status bar</li>
          <li>Only import files with known image extensions</li>
          <li>Use case insensitive file extension check to detect raw files</li>
          <li>Set saner defaults for processing raw files to fix Canon raw display</li>
          <li>Set preferences before loading session to fix settings for initial image load</li>
          <li>Apply orientation from EXIF images if image file has none</li>
          <li>Remove left over debug prints</li>
          <li>Add support for linear histogram display mode</li>
          <li>Support 'h' key to toggle linear/log histogram mode</li>
          <li>Ensure all events are emitted in main thread</li>
          <li>Remove use of deprecated GDK thread APIs</li>
          <li>Use GtkGrid instead of deprecated GtkTable</li>
          <li>Ensure status bar is constant size when scaling image</li>
          <li>Preserve scrollbar offsets when switching images</li>
          <li>Make metadata extraction more robust wrt missing fields</li>
          <li>Updated translations from transifex</li>
        </ul>
      </description>
    </release>
    <release version="0.5.1" codename="W Boson" date="2012-03-12">
      <description>
        <ul>
          <li>Update for compatibility with libgphoto 2.5 API callbacks</li>
          <li>Avoid warnings about deprecated glib2 mutex and condition variable APIs</li>
          <li>Directly disable viewfinder mode using config APIs</li>
          <li>Add support for triggering autofocus during preview with 'a' key</li>
          <li>Add support for manual focus drive in/out using '.' and ',' keys</li>
          <li>Refresh translations from transifex</li>
          <li>Import user contributed Italian translation</li>
          <li>Add missing translation markers on some strings</li>
        </ul>
        <p>Known issues:</p>
        <ul>
          <li>Compile triggers deprecation warnings about Gdk threads API usage and GtkTable API usage. Harmless but to be addressed in next release.</li>
        </ul>
      </description>
    </release>
    <release version="0.5.0" codename="Z Boson" date="2012-12-16">
      <description>
        <ul>
          <li>Switch to using LibRaw for loading raw images</li>
          <li>Allowing loading image preview from raw files</li>
          <li>Fix deadlock unmounting camera at startup</li>
          <li>Ensure camera is disconnected before exiting</li>
          <li>Sort image list by name instead of date</li>
          <li>Add support for rendering "onion skin" of image sequence</li>
          <li>Fix updating of range control values</li>
          <li>Fix filename generation for dual-format capture modes</li>
          <li>Fix deletion of dual-format images when exiting preview</li>
          <li>Refresh translations</li>
        </ul>
      </description>
    </release>
    <release version="0.4.1" codename="Gluon" date="2012-09-06">
      <description>
        <ul>
          <li>Fix leak of image pixbufs when changing image in session</li>
          <li>Keep toolbar directory in sync with session dir</li>
          <li>Fix leak when displaying image popups</li>
          <li>Fix leak when closing image popups</li>
          <li>Fix key bindings in session browser</li>
          <li>Add image histogram display</li>
          <li>Load libpeas introspection data for plugins</li>
          <li>Main plugin list in preferences</li>
          <li>Add object type checking in all APIs</li>
          <li>Fix image mask aspect ratio conversion to avoid locale problems</li>
          <li>Fix build on GTK &lt; 3.4</li>
          <li>Remove obsolete conditionals from GTK 2.x days</li>
          <li>Populate list of supported cameras in help menu dialog</li>
          <li>Add a simple man page</li>
          <li>Add accelerators for many menu options</li>
          <li>Fix unref of cairo surface objects</li>
          <li>Avoid GTK assertion when range is max-min is zero</li>
          <li>Avoid crash in control panel when updating after camera disconnect</li>
        </ul>
      </description>
    </release>
    <release version="0.4.0" codename="Higgs Boson" date="2012-07-08">
      <description>
        <ul>
          <li>Better use of GtkApplication class</li>
          <li>Add support for multiple windows &amp; cameras</li>
          <li>Add ability to sync capture/preview across windows</li>
          <li>Add preference to control whether cameras autoconnect at startup</li>
          <li>Add ability to apply aspect ratio masks to images</li>
          <li>Add key bindings for common actions (see README)</li>
          <li>Rewrite session browser widget to not use GtkIconView</li>
          <li>Add popup menu with session browser to allow open and delete of captured images</li>
          <li>Fix memory leak during preview</li>
          <li>Fix memory leak in session browser widget</li>
          <li>Add ability to DPMS-blank screen during capture for consistent lighting environment</li>
          <li>Add ability to render a focus point during preview</li>
          <li>Add ability to render grid lines during preview</li>
          <li>Merge "New session" and "Open session" toolbar buttons into one "Select session" drop down / menu</li>
          <li>Add custom icons for toolbar capture/preview buttons</li>
        </ul>
      </description>
    </release>
    <release version="0.3.3" codename="Muon neutrino" date="2012-04-25">
      <description>
        <ul>
          <li>Fix deadlock when starting app with glib >= 2.32</li>
          <li>Remove use of deprecated API calls</li>
          <li>Remove use of deprecated GTK widgets</li>
          <li>Fix debugging with glib >= 2.32</li>
          <li>Fix creation of session browser widget to avoid warnings</li>
          <li>Refresh translations</li>
          <li>Fix check for glib-compile-schemas binary</li>
        </ul>
      </description>
    </release>
    <release version="0.3.2" codename="Muon" date="2012-04-03">
      <description>
        <ul>
          <li>Major code style cleanup</li>
          <li>Mark all translatable strings in code &amp; UI files</li>
          <li>Register with Transifex for translations via Fedora team</li>
          <li>Pull in translations (German, Polish, Ukrainian, Japanese: full, Spanish, Chinese: partial).</li>
          <li>Add m4 macros for compiler warnings, missing from previous release dist.</li>
        </ul>
      </description>
    </release>
    <release version="0.3.1" codename="Tau neutrino" date="2012-02-13">
      <description>
        <ul>
          <li>Fix crash in handling camera control combo lists</li>
          <li>Add notice about need to set XDG_DATA_DIRS when installing into unusual directories</li>
          <li>Add workaround to avoid immediate crash if schemas were not found in XDG_DATA_DIRS</li>
          <li>Compile schema files after installation</li>
          <li>Fix crash updating widget sensitivity</li>
          <li>Fix crashes &amp; race conditions during capture of images</li>
          <li>Fix infinite preview error message popups which can hang the window manager</li>
          <li>Fix crash when retrying a failed connection attempt</li>
          <li>Fix thread locking when hiding status display</li>
          <li>Avoid running multiple threads for monitoring status</li>
          <li>Fix initial sensitivity of camera control panels</li>
          <li>Update README with new URLs for bugs/mailing lists</li>
        </ul>
      </description>
    </release>
    <release version="0.3.0" codename="Tau" date="2011-11-28">
      <description>
        <ul>
          <li>Switch to use Gtk3 for UI</li>
          <li>Switch to use GSettings for preferences</li>
          <li>Switch to use GtkBuilder for UI modelling</li>
          <li>Always enable GObject introspection</li>
          <li>Mandate libpeas for building</li>
          <li>Use gexiv2 for extracting metadata from images</li>
          <li>Rewrite internals for camera capture to be based on GIO async invocation, instead of explicitly using threads</li>
          <li>Automatically detect change of config on camera and propagate to UI (probably only works with Nikon cameras)</li>
          <li>Display basic image properties (aperture, iso, shutter speed, size and focal length) in a popup</li>
          <li>Remove support for HAL, in favour of udev</li>
          <li>Avoid 100% cpu burn with GVolumeMonitor / DBus bug</li>
          <li>Remember last image directory across restarts</li>
          <li>Default to $PICTURE_DIR/Capture for images, instead of creating a directory based on the camera model name</li>
          <li>Keep display of images in session even when no camera is connected</li>
          <li>Remove "Other PTP properties" from control panel to make the UI more scalable &amp; improve speed of loading the UI</li>
          <li>Preference to prevent deletion of images from camera after download</li>
          <li>Preference to allow preview to continue after image capture</li>
          <li>Continuously monitor for new images on the camera and download as they appear.</li>
          <li>Setup transient window hints to assist window manager in handling window placement</li>
          <li>Delete all website content which is now managed live</li>
          <li>Skip directories / special files when loading sessions</li>
        </ul>
      </description>
    </release>
    <release version="0.2.0" codename="Electron neutrino" date="2010-09-17">
      <description>
        <ul>
          <li>Better compatibility with cameras not supporting events</li>
          <li>Ability to unmount camera via GVFS</li>
          <li>Switch to libpeas for plugins</li>
          <li>Improved error reporting</li>
          <li>Fixed inifinite loops in event waiting</li>
          <li>Improved preview capability</li>
          <li>Fixed crash with udev</li>
        </ul>
      </description>
    </release>
    <release version="0.1.0" codename="Electron" date="2010-04-08">
      <description>
        <ul>
          <li>First release !</li>
        </ul>
      </description>
    </release>
  </releases>
  <translation type="gettext">entangle</translation>
</component>
